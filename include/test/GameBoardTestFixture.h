#ifndef GAMEBOARD_TEST_FIXTURE_H
#define GAMEBOARD_TEST_FIXTURE_H

#include <cppunit/extensions/HelperMacros.h>
#include <stdexcept>
#include "GameBoard.h"

class GameBoardTestFixture : public CppUnit::TestFixture
{
  private:
  public:
   CPPUNIT_TEST_SUITE(GameBoardTestFixture);
   CPPUNIT_TEST_EXCEPTION(BadImageFileName, std::runtime_error);
   CPPUNIT_TEST_EXCEPTION(BadBackGroundImageNameExpception, std::runtime_error);
   CPPUNIT_TEST_EXCEPTION(BadSongNameException, std::runtime_error);
   CPPUNIT_TEST_EXCEPTION(BadFallingImageName, std::runtime_error);
   CPPUNIT_TEST_EXCEPTION(BadMultiplierImageName, std::runtime_error);
   CPPUNIT_TEST_EXCEPTION(BadFontName, std::runtime_error);
   CPPUNIT_TEST_EXCEPTION(BadPowerUpImageName, std::runtime_error);
   CPPUNIT_TEST(playerImageCreationCheck);
   CPPUNIT_TEST(playerSpawnsInMiddle);
   CPPUNIT_TEST(updatePlayerPositionMovesPlayer);
   CPPUNIT_TEST(playerDoesNotMovePastBoundaries);
   CPPUNIT_TEST(updateScoreWithPositive);
   CPPUNIT_TEST(scoreNeverGoesBelowZero);
   CPPUNIT_TEST(FallingImageCreationCheck);
   CPPUNIT_TEST(FallingImageIsFallingOver1Second);
   CPPUNIT_TEST(FallingImageFallsBy100Over2SecondsWithSpeed50);
   CPPUNIT_TEST(FallingImageIncreasesScoreWhenReachesPlayer);
   CPPUNIT_TEST(FallingImageFallsBy120Over2SecondsWithSpeed60);
   CPPUNIT_TEST(GoodFallingImageWithScoreValue5IncreasesScoreTo5WhenCaught);
   CPPUNIT_TEST(TwoGoodFallingImagesWithScoreValues5And5IncreaseScoreTo10WhenBothAreCaught);
   CPPUNIT_TEST(GoodFallingImagesThatCrossTheXBoundaryAndAreNotCaughtDoNotUpdateScore);
   CPPUNIT_TEST(TwoGoodFallingImagesWithScoreValues5and5StartingAboveBoundaryIncreaseScoreBy10WhenBothAreCaught);
   CPPUNIT_TEST_SUITE_END();
  public:
   void setUp();
   void tearDown();
   void BadImageFileName();
   void BadBackGroundImageNameExpception();
   void BadSongNameException();
   void BadFallingImageName();
   void BadMultiplierImageName();
   void BadFontName();
   void BadPowerUpImageName();
   void playerImageCreationCheck();
   void playerSpawnsInMiddle();
   void updatePlayerPositionMovesPlayer();
   void playerDoesNotMovePastBoundaries();
   void updateScoreWithPositive();
   void scoreNeverGoesBelowZero();
   void FallingImageCreationCheck();
   void FallingImageIsFallingOver1Second();
   void FallingImageFallsBy100Over2SecondsWithSpeed50();
   void FallingImageIncreasesScoreWhenReachesPlayer();
   void FallingImageFallsBy120Over2SecondsWithSpeed60();
   void GoodFallingImageWithScoreValue5IncreasesScoreTo5WhenCaught();
   void TwoGoodFallingImagesWithScoreValues5And5IncreaseScoreTo10WhenBothAreCaught();
   void GoodFallingImagesThatCrossTheXBoundaryAndAreNotCaughtDoNotUpdateScore();
   void TwoGoodFallingImagesWithScoreValues5and5StartingAboveBoundaryIncreaseScoreBy10WhenBothAreCaught();
  private:
   GameBoard* board; 
};

#endif
