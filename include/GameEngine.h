#ifndef __GAMEENGINE_H
#define __GAMEENGINE_H

#include <allegro5/allegro.h>
#include <allegro5/allegro_image.h>
#include <allegro5/allegro_font.h>
#include <allegro5/allegro_audio.h>
#include <allegro5/allegro_ttf.h>
#include "GameBoard.h"
#include <cmath>
#include <ctime>
#include "HighScore.h"
#include <string>

///Drives the playing and creation of the Game
class GameEngine
{
  public:
   // GameEngine consructor
   GameEngine(int = 1200, int = 800, int = 120);
   // GameEngine destructor
   ~GameEngine();
   // the main function for running a game instance
   void gameSession();
   // based on the passed level choice the game level is run
   bool run(int);
  private:
   /** \brief user is able to enter a string following the completion
      of a level*/
   bool enterHighScore(int);
   /// a given levels HighScore list is displayed
   bool displayHighScores(int);
   /// an ALLEGRO_DISPLAY variable
   ALLEGRO_DISPLAY * display;
   /// ALLEGRO_EVENT_QUEUE to hold all events
   ALLEGRO_EVENT_QUEUE * eventQueue;
   /// ALLEGRO_TIMER for timed events
   ALLEGRO_TIMER * timer;
   /// the different keys available during game
   enum MyKeys { KEY_LEFT, KEY_RIGHT, KEY_SPACE, KEY_UP, KEY_DOWN };
   /// keeps track of whether a key has been pressed
   bool keys[2];
   /// A GameBoard object
   GameBoard* board ;
   /// the speed of the falling images 
   double speed;
   /// array to hold the 4 character images
   string characterImages[4];
   /// array to hold the 4 backgrounds
   string backGrounds[4];
   /// array to hold the 4 songs
   string songs[4];
   /// array to hold the 4 falling objects
   string fallingImage[4];
   /// array to hold the 4 varying startscreens
   ALLEGRO_BITMAP* startScreens[4];
   /// array to hold the 4 varying high score screens
   ALLEGRO_BITMAP* highScoresScreens[4];
   /// ALLEGRO_BITMAP for the screen when entering a high score
   ALLEGRO_BITMAP* enteringHighScoreScreen;
   /// a HighScores object for holding the current sessions High Scores
   HighScores scores; 
   
};

#endif
