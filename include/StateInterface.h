#ifndef __STATEINTERFACE_H
#define __STATEINTERFACE_H

#include<list>
#include "FallingImage.h"
#include"GameBoard.h"

class GameBoard;
/// Base State for playing the game
class StateInterface
{
  public:
   // returns the new position of the player
   virtual int updatePlayerPosition(int) const;
   /* handles the creation of falling images as well as the
      results of catching or missing the image */
   virtual void updateFallingImages(double,GameBoard&) const;
   /* handles the drawing of gameboard,the two players,
      falling images, multiplier indicator, active power up and score Text */ 
   virtual void draw(const GameBoard&) const;
   // virtual destructor for the State class
   virtual ~StateInterface(){}
};

#endif
