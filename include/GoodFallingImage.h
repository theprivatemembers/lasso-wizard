#ifndef __GOODFALLINGIMAGE_H
#define __GOODFALLINGIMAGE_H

#include "FallingImage.h"

/// FallingImage implementation for GoodImages
class GoodFallingImage : public FallingImage
{
  public:
   //GoodFallingImage constructor
   GoodFallingImage(const Point&, ALLEGRO_BITMAP*, int=0);
   // draws the GoodFallingImage to the screen
   void draw();
   // returns true if the image should still be visible
   bool isAlive(double dt);
   static void setSpeed(const Vector&);
  private:
   static Vector speed;
};

#endif
