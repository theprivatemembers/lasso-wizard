#ifndef HIGH_SCORE_H
#define HIGH_SCORE_H

#include <vector>
#include <string>
using namespace std;
/// score struct
/**
   contains three variables related to score (the level,
   the entered name and the score)
*/
struct score
{
   ///the level the score was achieved on as an int
   int level;
   ///the name of the player to whom the score belongs
   string name;
   ///the score value itself as an int
   int score;        
};

/// Class for dealing with the High Score data   
class HighScores
{
  public:
   // HighScores constructor
   HighScores();
   // HighScores destructor
   ~HighScores();
   // returns a vector of level scores
   const vector<score>& getScore(int);
   // adds a score to the scores vector
   void addScore(score, int);
   
  private:
   /* extracts the highscore info from the HighScores
      text file*/
   void initializeScores();
   /** vector of vectors for containg the scores for each
       individual level */
   vector<vector<score>> scores;
   // swaps the values of two scores
   void swap(score&, score&);
   // sorts the scores for a given level
   void sort(int);
   
};

#endif 
