#ifndef POINT_H
#define POINT_H

#include <iostream>
#include "Vector.h"

using namespace std ;

///\brief A Point class for handling coordinates with (x,y) values
/**
   The constructor for class Point requires (x,y) arguments, otherwise the value will be defaulted to (0,0)
   \see Vector
*/
class Point
{
  public:
   //this constructor works as both default constructor and constructor which
   //takes x and y coordinates
   Point (double xVal = 0, double yVal = 0) ;
   //+ operator overload for adding 2 points
   Point operator+ (const Point&) const ;
   //+ operator overload for adding a vector to a point
   Point operator+ (const Vector&) const ;
   
   double getX() const ; //returns x coordinate
   double getY() const ; //returns y coordinate
   //output operator overload
   friend ostream& operator<< (ostream& out, const Point&) ;
   
  private:
   ///The value of the x coordinate
   double x ;

   ///The value of the y coordinate
   double y ;
} ;

#endif
