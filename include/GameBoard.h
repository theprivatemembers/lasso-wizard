#ifndef GAMEBOARD_H
#define GAMEBOARD_H

#include <stdexcept>
#include "FallingImage.h"
#include "GoodFallingImage.h"
#include "StaticFallingImage.h"
#include "Point.h"
#include "Text.h"
#include <allegro5/allegro.h>
#include <allegro5/allegro_image.h>
#include <allegro5/allegro_font.h>
#include <allegro5/allegro_ttf.h>
#include <allegro5/allegro_audio.h>
#include <allegro5/allegro_acodec.h>
#include <string>
#include <list>
#include "Text.h"
#include "StateInterface.h"
#include "PowerUpFallingImage.h"


class StateInterface;
typedef StateInterface State;
using namespace std;

///Class that handles everything required for a game
class GameBoard
{
  public:
   GameBoard(std::string, std::string, std::string, std::string,  double = 50.0);
   
   // GameBoard destructor
   ~GameBoard();
   
   // calls the current States draw function
   void draw();
   
   // returns the current score font
   ALLEGRO_FONT* getScoreFont() const;

   // returns the current background image
   ALLEGRO_BITMAP* getBackgroundImage() const;

   // returns the current player Image
   ALLEGRO_BITMAP* getPlayerImage() const;
   
   // sets the current PowerUp state
   void setPowerUpState(int);
   
   // returns the current falling Image
   FallingImage* getFallingImage();
   
   // returns the position of the player
   int getPlayerPosition() const;
   
   // sets the position of the player
   void updatePlayerPosition(int);
   
   // updates the score
   void updateScore(int);
   
   // returns the current score
   int getScore() const;
   
   // sets the score value
   void setScore(int);
   
   // returns the players height
   double getPlayerHeight();
   
   // returns the players width
   double getPlayerWidth();
   
   // updates the falling images based on the current state
   void updateFallingImages(double);
   
   // adds a falling image to the fallingImages list
   void addFallingImage();
   
   // adds a PowerUp to the fallingImages list
   void addPowerUpFallingImage();
   
   // deletes an image from the fallingImages list and returns the trailing pointer
   list<FallingImage*>::iterator removeFallingImage(list<FallingImage*>::iterator);
   
   // returns the length of the current song instance
   double getMusicLength() const;
   
   // returns the value of the current multiplier
   int getMultiplier() const;
   
   // sets the current multiplier value
   void setMultiplier(int);
   
   // returns the current number of consecutive catches
   int getCaughtCount();
   
   // sets the current number of consecutive catches
   void setCaughtCount(int);
   
   // returns the current count down value of the PowerUp
   double getPowerUpCountDown() const;
   
   // returns the list of falling Images
   list<FallingImage*>& getFallingImages();
   
   // returns the list of const falling Images
   const list<FallingImage*>& getFallingImages() const;
   
   // returns the current multiplier image
   ALLEGRO_BITMAP * getMultiplierImage(int) const;

   // set the MultiplierImageNames
   void setMultiplierImageNames(string[]);

   // set the FontName
   void setFontName(string);

   // set the PowerUpImageName
   void setPowerUpImageName(string);
   
   // mutes the current song instance
   void muteMusic(bool);
   
   // starts the music instance for a given level
   void startMusic();

   // stops and restarts the current song instance
   void pauseAndRestart();
      
   // returns true if the fallingImages list is ready for another image
   bool readyForImage();

   // returns true if the current state is a PowerUp state
   bool isPowerUpState();
   
   // reduces the PowerUp countdown
   void decrementCountdown(double);
   
  private:
   /// ALLEGRO_BITMAP for the current player image
   ALLEGRO_BITMAP* playerImage;
   /// ALLEGRO_BITMAP for the current background image
   ALLEGRO_BITMAP* background;
   /// list to hold the fallingImages
   list<FallingImage*> fallingImages;
   /// the current position of the player
   int playerPosition;
   /// the level score
   int score;
   /// the speed of the falling images
   int imageSpeed;
   /// tracks the number of consecutive catches
   int caughtCount;
   /// the current multiplier count
   int multiplier;
   /// tracks the length the PowerUp should be active
   double powerUpCountdown;
   /// the current falling image title
   string fallingImageName;
   /// the name of the PowerUp image
   string powerUpImageName;
   /// the font used for displaying the score
   ALLEGRO_FONT* scoreFont;
   /// the current song
   ALLEGRO_SAMPLE* song;
   /// the current song instance
   ALLEGRO_SAMPLE_INSTANCE* songInstance;
   /// the ALLEGRO_MIXER needed to generate audio
   ALLEGRO_MIXER* ourMixer;
   /// true if the current song is playing
   bool isPlaying;
   /// array for holding the 5 multiplier images
   ALLEGRO_BITMAP* multi[5];
   /// the current active state
   State * currentState;
   /// the state for no power up
   State * noPowerUp;
   /// the state for reverse movement
   State * reverseMovement;
   /// the state for having double players
   State * doubleDude;
   /// the state for double base multiplier
   State * doubleMultiplier;
};

#endif
