#ifndef __FALLINGIMAGE_H
#define __FALLINGIMAGE_H
#include <allegro5/allegro.h>
#include <allegro5/allegro_image.h>
#include "Point.h"
#include "Vector.h"

///Abstract base class for images falling down the screen
class FallingImage
{
  public:
   // FallingImage constructor
   FallingImage(const Point&, ALLEGRO_BITMAP*, int=0);
   // FallingImage destructor
   virtual ~FallingImage();
   // returns the origin of the falling image
   Point getOrigin();
   // set the origin of the FallingImage
   void setOrigin(const Point&);
   // move the Image based on the speed
   void translate(const Vector&);
   // returns the height of the FallingImage
   double getHeight();
   // returns the width of the FallingImage
   double getWidth();
   // returns the score value of the Image
   int getScoreValue();
   ///virtual draw funtion to be handled in the various FallingImage types
   virtual void draw() = 0;
   ///virtual isAlive funtion to be handled in the various FallingImage types
   virtual bool isAlive(double dt) = 0;
   // sets the score of value of the Image
   void setScoreValue(const int);
  protected:
   /// Holds the (x,y) coordinate for top-left corner of the image
   Point origin;
   /// Pointer to the actual image which is
   ALLEGRO_BITMAP *image;
   /// The score value of the FallingImage
   int scoreValue;
};

#endif
