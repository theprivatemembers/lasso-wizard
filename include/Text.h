#ifndef __TEXT_H
#define __TEXT_H

#include <iostream>
#include <allegro5/allegro.h>
#include <allegro5/allegro_image.h>
#include <allegro5/allegro_font.h>
#include <allegro5/allegro_ttf.h>
#include <string>
#include "Point.h"

///Contains all relevant information for drawing text using Allegro
class Text
{
  public:
   // Text class constructor
   Text(string, ALLEGRO_FONT*, int, int, int, const Point&);
   // Text class destructor
   ~Text();
   // returns the Text origin
   Point getOrigin();
   // sets the origin of the Text
   void setOrigin(const Point&);
   // print function for displaying the Text
   void print();
   // sets the text variable to the passed string
   void setText(string);

  private:
   /// the font style of the text
   ALLEGRO_FONT* font;
   /// the colour of the font
   ALLEGRO_COLOR* color;
   /// text to be displayed
   string text;
   /// location of the text
   Point origin;
};

#endif
