#ifndef VECTOR_H
#define VECTOR_H

#include <iostream>

using namespace std ;

///\brief A Vector class for handling relative coordinates with (x,y) values.
class Vector
{
  public:
   Vector (double xVal = 0, double yVal = 0) ;// Vector constructor
   Vector operator+ (const Vector&) const ; // + operator overload
   Vector operator* (const double) const ; // for multiplying a vector by a time amount
   double getX() const ; // returns x coordinate
   double getY() const ; // returns y coordinate
   void setX(double) ; // sets x coordinate
   void setY(double) ; // sets y coordinate
   // output operator overload
   friend ostream& operator<< (ostream& out, const Vector&) ;
   
  private:
   /// The value of the relative x coordinate
   double x ;

   /// The value of the relative y coordinate
   double y ;
} ;

#endif
   
