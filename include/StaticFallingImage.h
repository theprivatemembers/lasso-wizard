#ifndef STATIC_FALLING_IMAGES_H
#define STATIC_FALLING_IMAGES_H

#include "FallingImage.h"
/// FallingImage implementation for Static Images (Score Popups)
class StaticFallingImage : public FallingImage
{
  public:
   // StaticFallingImage constructor
   StaticFallingImage(const Point&, ALLEGRO_BITMAP*, double);
   // returns true if the images lifetime is greater than 0
   bool isAlive(double dt);
   // reduces the lifetime of the Image
   void decrementLife(double);
   // draws the image to the screen
   void draw();

  private:
   /// how the long the image will be displayed
   double lifetime;
};

#endif
