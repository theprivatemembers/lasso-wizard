#ifndef __DOUBLEMULTIPLIERSTATE_H
#define __DOUBLEMULTIPLIERSTATE_H

#include "StateInterface.h"
/// PowerUp State for having a double valued score multiplier
class DoubleMultiplierState: public StateInterface
{
  public:
   //DoubleMultiplierState default constructor
   DoubleMultiplierState();
   //DoubleMultiplierState destructor
   ~DoubleMultiplierState();
   /* handles the drawing of gameboard,the two players,
      falling images, multiplier indicator, active power up and score Text */ 
   void updateFallingImages(double,GameBoard&) const;
   /* handles the drawing of gameboard, player, falling images,
      multiplier indicator and Text */
   void draw(const GameBoard&) const;
  private:
   ///image for indicating active power up
   ALLEGRO_BITMAP * activePowerUpImage;
};

#endif
