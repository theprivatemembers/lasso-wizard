#ifndef __POWER_UP_FALLING_IMAGE
#define __POWER_UP_FALLING_IMAGE

#include "FallingImage.h"

/// FallingImage implementation for PowerUps
class PowerUpFallingImage : public FallingImage
{
  public:
   // fall speed of the PowerUp image
   static Vector speed;
   // PowerUpFallingImage constructor
   PowerUpFallingImage(const Point&, ALLEGRO_BITMAP*);
   // draws the PowerUpFallingImage
   void draw();
   /* returns true if the PowerUp Image should still be
      displayed*/
   bool isAlive(double dt);
   
};


#endif
