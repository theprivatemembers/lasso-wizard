#ifndef __REVERSEMOVEMENTSTATE_H
#define __REVERSEMOVEMENTSTATE_H

#include"StateInterface.h"
/// PowerUp State for having reversed movement controls
class ReverseMovementState : public StateInterface
{
  public:
   //ReverseMovementState default constructor
   ReverseMovementState();
   //ReverseMovementState destructor
   ~ReverseMovementState();
   // updates the players position in a reversed manner
   int updatePlayerPosition(int) const;
   /* handles the drawing of gameboard,the two players,
      falling images, multiplier indicator, active power up and score Text */ 
   void draw(const GameBoard&) const;
  private:
   ///image for indicating active power up
   ALLEGRO_BITMAP * activePowerUpImage;
};

#endif
