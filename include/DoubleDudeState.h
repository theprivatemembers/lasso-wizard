#ifndef __DOUBLEDUDESTATE__H
#define __DOUBLEDUDESTATE__H

#include "StateInterface.h"
/// PowerUp State for double player
class DoubleDudeState : public StateInterface
{
  public:
   //DoubleDudeState default constructor
   DoubleDudeState();
   //DoubleDudeState destructor
   ~DoubleDudeState();
   /* handles the drawing of gameboard,the two players,
      falling images, multiplier indicator, active power up and score Text */ 
   void draw(const GameBoard&) const;
   /* handles the creation of falling images as well as the
      results of catching or missing the image */
   void updateFallingImages(double,GameBoard&) const;
  private:
   ///image for indicating active power up
   ALLEGRO_BITMAP * activePowerUpImage;
};


#endif
