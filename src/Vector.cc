#include <iostream>
#include "Vector.h"

using namespace std ;

///Initializes a Vector.
/**
   If no values are given, the Vector object will be initialized with value (0,0)
   \param _x The relative x coordinate
   \param _y The relative y coordinate
*/
Vector::Vector (double _x, double _y) : x(_x), y(_y) {}

///Allows addition of a Vector to a Vector in the form Vector + Vector
/**
   Adds rhs to *this.
   \return The Vector sum of *this + rhs
   \param rhs The right-hand side operand of the addition (*this + rhs)
*/
Vector Vector::operator+ (const Vector& rhs) const
{
   return Vector(x + rhs.x, y + rhs.y) ;
}

///Allows multiplication of a Vector by a scalar value in the form Vector * scalar
/**
   Multiplies *this by scalar
   \return The Vector product of (*this) * rhs
   \param scalar A scalar value stored as a double.  The right-hand operand of the multiplication (*this * rhs)
*/
Vector Vector::operator* (const double scalar) const
{
   return Vector(x * scalar, y * scalar) ;
}

///Relative x coordinate getter
/**
   \return The relative x coordinate as a double
*/
double Vector::getX() const
{
   return x ;
}

///Relative y coordinate getter
/**
   \return The relative y coordinate as a double
*/
double Vector::getY() const
{
   return y ;
}

///Relative x coordinate setter
/**
   Sets the relative x coordinate to a new value
   \param newX The new relative x coordinate (a double)
*/
void Vector::setX(double newX)
{
   x = newX ;
}

///Relative y coordinate setter
/**
   Sets the relative y coordinate to a new value
   \param newY The new relative y coordinate (a double)
*/
void Vector::setY(double newY)
{
   y = newY ;
}

///Output operator overload, used in the form ostream << Vector
/**
   Overloads the output operator(<<) for easy printing
   \return The ostream which was passed into the function as a parameter; allows cascading
   \param out An ostream which is the lefthand operand for the << operator; usually cout
   \param rhs A Vector which is the righthand operand for the << operator
*/
ostream& operator<< (ostream& out, const Vector& rhs)
{
   out << "->(" << rhs.x << "," << rhs.y << ")" ;

   return out ; //for cascading
}
