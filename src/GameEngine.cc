#include "GameEngine.h"
#include <fstream>

///Constructor for GameEngine
/**
   \param width will be the width of the created display
   \param height will be the height of the created display
   \param fps will be the number of frames per second of the game

  This constructor installs and initializes all of the required Allegro addons. It also registers the event sources. And stores the file names for the images. Loads the start and high score screen images. 
 */
GameEngine::GameEngine(int width, int height, int fps)
{
   //************************ ADDON INSTALL/INITIALIZATION ********************//
   al_init();
   al_init_image_addon();
   al_install_keyboard();
   al_init_font_addon();
   al_init_ttf_addon();
   al_install_audio();
   al_init_acodec_addon();
   display = al_create_display(width,height);
   eventQueue = al_create_event_queue() ;
   timer = al_create_timer(1.0/fps);

   //initialize key array
   keys[0] = keys[1] = false;
   speed = 300;
   
   //************************ EVENT SOURCE REGISTRATION *********************//
   al_register_event_source(eventQueue, al_get_display_event_source(display));
   al_register_event_source(eventQueue, al_get_timer_event_source(timer));
   al_register_event_source(eventQueue, al_get_keyboard_event_source());

   
   //************************ IMAGES SETUP *********************************//
   characterImages[0] = "./image/patrick.png";
   characterImages[1] = "./image/darren.png";
   characterImages[2] = "./image/jacob.png";
   characterImages[3] = "./image/jon.png";

   backGrounds[0] = "./image/patrickBackground.jpg";
   backGrounds[1] = "./image/darrenBackground.jpg";
   backGrounds[2] = "./image/jacobBackground.jpg"; 
   backGrounds[3] = "./image/jonBackground.jpg";

   songs[0] = "./songs/BitBurner8bitSweater.wav" ;
   songs[1] = "./songs/theWorld.wav" ;
   songs[2] = "./songs/Fleshdiet_Snow.wav";
   songs[3] = "./songs/Ryuushunhayashi_Reflection.wav";

   fallingImage[0] = "./image/beer.png";
   fallingImage[1] = "./image/submarine.png";
   fallingImage[2] = "./image/snowflake.png";
   fallingImage[3] = "./image/parrot.png";
   
   if (!(startScreens[0] = al_load_bitmap("./image/stageSelect0.jpg")))
      throw (std::runtime_error("Bad Filename: ./image/stageSelect0.jpg Does Not Exist"));
   
   if (!(startScreens[1] = al_load_bitmap("./image/stageSelect1.jpg")))
      throw (std::runtime_error("Bad Filename: ./image/stageSelect1.jpg Does Not Exist"));
   
   if (!(startScreens[2] = al_load_bitmap("./image/stageSelect2.jpg")))
      throw (std::runtime_error("Bad Filename: ./image/stageSelect2.jpg Does Not Exist"));
   
   if (!(startScreens[3] = al_load_bitmap("./image/stageSelect3.jpg")))
      throw (std::runtime_error("Bad Filename: ./image/stageSelect3.jpg Does Not Exist"));
   
   if (!(highScoresScreens[0] = al_load_bitmap("./image/highScoresBackground0.jpg")))
      throw (std::runtime_error("Bad Filename: ./image/highScoresBackground0.jpg Does Not Exist"));
   
   if (!(highScoresScreens[1] = al_load_bitmap("./image/highScoresBackground1.jpg")))
      throw (std::runtime_error("Bad Filename: ./image/highScoresBackground1.jpg Does Not Exist"));
   
   if (!(highScoresScreens[2] = al_load_bitmap("./image/highScoresBackground2.jpg")))
      throw (std::runtime_error("Bad Filename: ./image/highScoresBackground2.jpg Does Not Exist"));
      
   if (!(highScoresScreens[3] = al_load_bitmap("./image/highScoresBackground3.jpg")))
      throw (std::runtime_error("Bad Filename: ./image/highScoresBackground3.jpg Does Not Exist"));
   
   if (!(enteringHighScoreScreen = al_load_bitmap("./image/enterYourName.jpg")))
      throw (std::runtime_error("Bad Filename: ./image/enterYourName.jpg Does Not Exist"));

   srand(time(NULL));
   
}

///Destructor for GameEngine
/**
   This destructor will destroy all Allegro Objects and the GameBoard 
 */
GameEngine::~GameEngine()
{
   
   if(display)
   {
      al_destroy_display(display);
      display = NULL;
   }

   if(timer)
   {
      al_destroy_timer(timer);
      timer = NULL;
   }

   if(eventQueue)
   {
      al_destroy_event_queue(eventQueue);
      eventQueue = NULL;
   }
   if(board)
   {
      delete board;
      board = NULL;
   }

   for (int i = 0; i < 4; i++)
   {
      al_destroy_bitmap(startScreens[i]);
      startScreens[i] = NULL;
   }

   for (int i = 0; i < 4; i++)
   {
      al_destroy_bitmap(highScoresScreens[i]);
      highScoresScreens[i] = NULL;
   }
   al_destroy_bitmap(enteringHighScoreScreen);
   
}

///Runs the Game
/**
   \param levelChoice is an int representing the level that is being played
   \return a bool representing whether run completed successfully or not

   This function takes care of running the game. It captures the keyboard presses, updates the images and draws everything. 
 */
bool GameEngine::run(int levelChoice)
{
   double playingTime = board -> getMusicLength();
   double stopTime = playingTime - (800/speed); //This is the amount of time it takes a shape to reach the bottom of the screen so this is the time in which shapes should stop spawning
   bool doExit = false;
   bool redraw = false;

   bool isPaused = false; 
   
   double crtTime = al_current_time();
   double prevTime = al_current_time();
   double timePassed = 0;
   
   board -> startMusic();
   al_start_timer(timer);
   
   while(!doExit)
   {
      ALLEGRO_EVENT event;
      al_wait_for_event(eventQueue, &event);

      if (timePassed > playingTime)
	 break;
      

      if((board -> readyForImage()) and (timePassed < (stopTime)) and (!isPaused)) 
      {
	 int imageType = rand() % 20;
	 if (imageType == 0 and !(board -> isPowerUpState()))
	    board -> addPowerUpFallingImage();
	 else
	    board -> addFallingImage();
      }

      if(isPaused)
      {
	 crtTime = al_current_time();
	 prevTime = crtTime;
      }
      
      //Start of -if- structure for events. 
      if((event.type == ALLEGRO_EVENT_TIMER) and (!isPaused))
      {
	 crtTime = al_current_time();
	 
	 if(keys[KEY_LEFT]){
	    board -> updatePlayerPosition(-150); //move player left
	    keys[KEY_LEFT] = false;
	 }
	 else if(keys[KEY_RIGHT]){
	    board -> updatePlayerPosition(150); //move player right
	    keys[KEY_RIGHT] = false;
	 }

	 board -> updateFallingImages(crtTime - prevTime);

	 redraw = true;
	 timePassed += (crtTime - prevTime);
	 prevTime = crtTime;
      }
      

      else if(event.type == ALLEGRO_EVENT_DISPLAY_CLOSE)
      {
	 if(isPaused)
	    board -> pauseAndRestart();
	 return false;
      }
      else if(event.type == ALLEGRO_EVENT_KEY_DOWN)
      {
	    switch(event.keyboard.keycode)
	    {
	       case ALLEGRO_KEY_LEFT:
		  if (!isPaused)
		     keys[KEY_LEFT] = true;
		  break;
		  
	       case ALLEGRO_KEY_RIGHT:
		  if(!isPaused)
		     keys[KEY_RIGHT] = true;
		  break;
		  
	       case ALLEGRO_KEY_SPACE:
		  isPaused = !isPaused;
		  board -> pauseAndRestart();
		  break;
	       case ALLEGRO_KEY_ESCAPE:
		  return true;
		  break;

		  
	       default:
		  break;
	    }
	 
      }

      if(redraw and al_is_event_queue_empty(eventQueue));
      {
	 redraw = false;
	 board -> draw();
      }
   }

   if(!enterHighScore(levelChoice))
      return false;
   
   if(!displayHighScores(levelChoice))
      return false;
   
    return true;
	 
}

///Function that sets the game up
/**
   This function displays the start screens and sets the GameBoard up based on the level choice. 
 */
void GameEngine::gameSession()
{
   int curLevel=0;
   bool doExit = false;
   bool redraw = false;
   bool play = false;

   al_draw_bitmap(startScreens[0], 0, 0 ,0);
   al_flip_display();
   while(!doExit)
   {
      ALLEGRO_EVENT event;
      al_wait_for_event(eventQueue, &event);

      if(event.type == ALLEGRO_EVENT_DISPLAY_CLOSE)
      {
	 doExit = true; 
	 board = NULL;
      }

      else if(event.type == ALLEGRO_EVENT_KEY_DOWN)
      {
	 switch(event.keyboard.keycode)
	 {
	    case ALLEGRO_KEY_UP:
	       curLevel = (curLevel - 1) % 4;
	       if (curLevel < 0)
		  curLevel = 3;
	       redraw = true;
	       break;
		  
	    case ALLEGRO_KEY_DOWN:
	       curLevel = (curLevel + 1) % 4;
	       redraw = true;
	       break;

	    case ALLEGRO_KEY_ENTER:
	       play = true;
	 }
	 
      }

      if(redraw and al_is_event_queue_empty(eventQueue))
      {
	 al_draw_bitmap(startScreens[curLevel], 0, 0 ,0);
	 al_flip_display();
	 redraw = false;
      }

      if (play)
      {
	 board = new GameBoard(characterImages[curLevel], backGrounds[curLevel], songs[curLevel], fallingImage[curLevel], speed);

	 if (!run(curLevel))
	    doExit = true;


	 else
	 {
	    al_stop_timer(timer);

	    al_flush_event_queue(eventQueue);
	
	    delete board; //dont delete until here because enterHighScore needs it!
	    board = NULL;

	    al_flush_event_queue(eventQueue);
	 
	    play = false;

	    al_draw_bitmap(startScreens[curLevel], 0, 0 ,0);
	    al_flip_display();
	 }
	 
      }
   }
   
}

///Allows Entering of Name for High Score
/**
   \param curLevel is an int representing the level that was chosen to be played
   \return a bool representing whether the name was entered correctly or not
   Displays the entering high score screen and captures the keys for entering of the name and stores it in HighScores
 */
bool GameEngine::enterHighScore(int curLevel)
{
   std::string name = "" ;
   al_draw_bitmap(enteringHighScoreScreen,0,0,0);
   al_flip_display();

   al_set_new_bitmap_flags(ALLEGRO_MEMORY_BITMAP);
   ALLEGRO_FONT * nameFont;
   if (!(nameFont = al_load_font("./fonts/ColorsOfAutumn.ttf", 128,0)))
      throw (std::runtime_error("Bad Filename: ./fonts/ColorsOfAutumn.ttf Does Not Exist"));
   al_set_new_bitmap_flags(ALLEGRO_VIDEO_BITMAP);
   
   Text nameText(name, nameFont, 0,0,0, Point(80,350));
   
   while(true)
   {
      ALLEGRO_EVENT event;
      al_wait_for_event(eventQueue, &event);

      if(event.type == ALLEGRO_EVENT_DISPLAY_CLOSE)
	 return false; 
      
      else if(event.type == ALLEGRO_EVENT_KEY_CHAR)
      {
	 if(32 <= event.keyboard.unichar and event.keyboard.unichar <= 125
	    and name.length() < 10)     
	    name.push_back((char)(event.keyboard.unichar));
	 
	 else if(event.keyboard.keycode == ALLEGRO_KEY_BACKSPACE)
	 {
	    if(name.length() > 0)
	       name.pop_back();
	 }
	 
      }

      else if(event.type == ALLEGRO_EVENT_KEY_DOWN)
      {
	 if(event.keyboard.keycode == ALLEGRO_KEY_ESCAPE)
	    return true;
      }
      
      else if(event.type == ALLEGRO_EVENT_KEY_UP)
      {
	 if(event.keyboard.keycode == ALLEGRO_KEY_ENTER)
	    break;
      }
    

      al_clear_to_color(al_map_rgb(0,0,0));
      al_draw_bitmap(enteringHighScoreScreen,0,0,0);
      nameText.setText(name);
      nameText.print();
      al_flip_display();

   }

   score newScore;
   newScore.level = curLevel;
   newScore.name = name;
   newScore.score = board -> getScore();

   scores.addScore(newScore, curLevel);
   return true;
}

///Displays the highscores for all levels
/**
   \param curLevel is an int representing the level that was just played
   \return a bool representing how the High Score Screen was exited.

   This function displays all of the high scores for all the levels starting on the level that was just played. It captures the keys to allow switching between screens. 
 */
bool GameEngine::displayHighScores(int curLevel)
{
   bool redraw = true;
   al_set_new_bitmap_flags(ALLEGRO_MEMORY_BITMAP);
   ALLEGRO_FONT* highScoreFont;
   if (!(highScoreFont = al_load_font("./fonts/ColorsOfAutumn.ttf", 64,0)))
      throw (std::runtime_error("Bad Filename: ./fonts/ColorsOfAutumn.ttf Does Not Exist"));
   al_set_new_bitmap_flags(ALLEGRO_VIDEO_BITMAP);

   al_flush_event_queue(eventQueue);
   
   al_draw_bitmap(highScoresScreens[curLevel],0,0,0);
   al_flip_display();
   while(true)
   {
      ALLEGRO_EVENT event;
      al_wait_for_event(eventQueue, &event);

      if(event.type == ALLEGRO_EVENT_DISPLAY_CLOSE)
      {
	 al_destroy_font(highScoreFont);
	 return false;
      }
      
      else if(event.type == ALLEGRO_EVENT_KEY_DOWN)
      {
	 switch(event.keyboard.keycode)
	 {
	    case ALLEGRO_KEY_LEFT:
	       curLevel = (curLevel - 1) % 4;
	       if (curLevel < 0)
		  curLevel = 3;
	       redraw = true;
	       break;
		  
	    case ALLEGRO_KEY_RIGHT:
	       curLevel = (curLevel + 1) % 4;
	       redraw = true;
	       break;

	    case ALLEGRO_KEY_ESCAPE:
	       al_destroy_font(highScoreFont);
	       return true;
	       break;
	 }
      }

      else if(redraw and al_is_event_queue_empty(eventQueue))
      {
	 al_draw_bitmap(highScoresScreens[curLevel],0,0,0);
	 vector<score> highScoresTemp = scores.getScore(curLevel);
	 vector<Text*> textTemp ;
	 for(size_t i = 0; i < highScoresTemp.size(); i++)
	 {
	    std::string name = highScoresTemp[i].name;
	   std:string score = std::to_string(highScoresTemp[i].score);
	    Text* temp1 = new Text(name, highScoreFont, 0,0,0, Point(250,100*(i+1)));
	    Text* temp2 = new Text(score, highScoreFont, 0,0,0, Point(850,100*(i+1)));
	    textTemp.push_back(temp1);
	    textTemp.push_back(temp2);
	 }
	 for(size_t i = 0; i < textTemp.size(); i++)
	 {
	    textTemp[i] -> print();
	    delete textTemp[i];
	    
	 }
	 al_flip_display();
	 redraw = false;

      }

   }
   al_destroy_font(highScoreFont);
   return true;
}
