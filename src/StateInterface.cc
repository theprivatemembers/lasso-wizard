#include "StateInterface.h"

///calculates the amount by which the player position should be updated
/**
   \param movement an int representing the distance the player should move
   \return the amount by which the player position should by updated (this will be added to player position)
*/
int StateInterface::updatePlayerPosition(int movement) const
{
   return movement;
}

///updates the list of FallingImage pointers contained in the GameBoard currently in play
/**
   Handles catching logic
   \param dt a double representing the change in time
   \param board the GameBoard currently in play
*/
void StateInterface::updateFallingImages(double dt, GameBoard& board) const
{
   bool caught = false;
   Point temp;

   board.decrementCountdown(dt);
   if (board.getPowerUpCountDown() <= 0)
      board.setPowerUpState(0);
   
   auto iter = board.getFallingImages().begin();
   while(iter != board.getFallingImages().end())
   {
      // isAlive will translate/decrement lifetime of a FallingImage and then return whether it is still alive
      if(!((*iter) -> isAlive(dt))) // Image has hit the Y-Axis (if it is a GoodFallingImage or PowerUpFallingImage) or lifetime has expired (if it is a static falling image)
      {
	 
	 if (((*iter) -> getOrigin().getX() == board.getPlayerPosition()) and ((*iter) -> getScoreValue() > 0)) // if image was caught and is a GoodFallingImage
	 {
	    board.setCaughtCount(board.getCaughtCount()+1);
      	    if ((board.getCaughtCount() % 10 == 0) and (board.getMultiplier() != 8) and (board.getCaughtCount() != 0))
      	       board.setMultiplier(board.getMultiplier() * 2);
	    
      	    board.muteMusic(false); // turn audio back on in case it was muted
      	    board.updateScore((*iter)->getScoreValue());
	    temp = (*iter) -> getOrigin();
	    caught = true;
	 }
	
	 else if ((*iter) -> getScoreValue() > 0) // if GoodFallingImage wasn't caught
      	 {
      	    board.setCaughtCount(0);
      	    board.setMultiplier(1);
      	    board.muteMusic(true); // mute audio
      	 }

	 else if ((((*iter) -> getOrigin().getX()) == board.getPlayerPosition()) and ((*iter) -> getScoreValue() < 0)) // If image was caught and is a PowerUpFallingImage
	    board.setPowerUpState((*iter) -> getScoreValue());

	 iter = board.removeFallingImage(iter);
      	 continue;
      }
      iter++;   
   }

   if(caught)
   {
      ALLEGRO_BITMAP* newImage;
      switch(board.getMultiplier())
      {
	 case 1:
	    if (!(newImage = al_load_bitmap("./image/scorePopUp5.png")))
	       throw (std::runtime_error("Bad Filename: ./image/scorePopUp5.png Does Not Exist"));
	    
	    board.getFallingImages().push_front(new StaticFallingImage(temp, newImage, 0.5));
	    break;
	 case 2:
	    if (!(newImage = al_load_bitmap("./image/scorePopUp10.png")))
	       throw (std::runtime_error("Bad Filename: ./image/scorePopUp10.png Does Not Exist"));
	    
	    board.getFallingImages().push_front(new StaticFallingImage(temp, newImage, 0.5));
	    break;
	 case 4:
	    if (!(newImage = al_load_bitmap("./image/scorePopUp20.png")))
	       throw (std::runtime_error("Bad Filename: ./image/scorePopUp20.png Does Not Exist"));
	    
	    board.getFallingImages().push_front(new StaticFallingImage(temp, newImage, 0.5));
	    break;
	 case 8:
	    if (!(newImage = al_load_bitmap("./image/scorePopUp40.png")))
	       throw (std::runtime_error("Bad Filename: ./image/scorePopUp40.png Does Not Exist"));
	    board.getFallingImages().push_front(new StaticFallingImage(temp, newImage, 0.5));
	    break;
      }
   }
}

///Draws all of the items in GameBoard
/**
   \param board the GameBoard currently in play

   Handles the drawing of the Background, the player Image, the multiplier image, the Score
   and the falling Images.
*/
void StateInterface::draw(const GameBoard& board) const
{
   al_draw_bitmap(board.getBackgroundImage(), 0, 0, 0);
   al_draw_bitmap(board.getPlayerImage(), board.getPlayerPosition(), 600, 0);

   al_draw_bitmap(board.getMultiplierImage(board.getMultiplier()), 760, 470, 0);
   const list<FallingImage*>& fallingImages = board.getFallingImages();
   for(auto iter : fallingImages)
      iter -> draw();
   
   Text((to_string(board.getScore())), board.getScoreFont(),0, 139, 69,Point(800,400)).print();
   
   al_flip_display();
}
