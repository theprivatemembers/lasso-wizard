#include "FallingImage.h"
#include <allegro5/allegro_image.h>
#include <iostream>

///FallingImage constructor
/**
   \param _origin the Point to be used as the origin of the FallingImage
   \param picture the bitmap to be used as an ALLEGRO_BITMAP * of the FallingImage
   \param _scoreValue the int to be used as the scoreValue of the FallingImage
 */
FallingImage::FallingImage(const Point& _origin, ALLEGRO_BITMAP* picture, int _scoreValue) : origin(_origin), scoreValue(_scoreValue)
{
   image = picture;
}

///FallingImage destructor
FallingImage::~FallingImage()
{
   al_destroy_bitmap(image);
}

///origin getter
/**
   \return the origin as a Point
 */
Point FallingImage::getOrigin()
{
   return origin ;
}

///origin setter
/**
   \param newOrigin the new coordinates for the origin as a Point
 */
void FallingImage::setOrigin(const Point& newOrigin)
{
   origin = newOrigin;
}

///translates a shape by adding a Vector to its origin
/**
   \param speed the Vector representing the change in position of the origin
 */
void FallingImage::translate(const Vector& speed)
{
   origin = origin + speed;
}

///height getter
/**
   \return the height of the FallingImage
 */
double FallingImage::getHeight()
{
   return al_get_bitmap_height(image);
}

///width getter
/**
   \return the width of the FallingImage
 */
double FallingImage::getWidth()
{
   return al_get_bitmap_width(image);
}

///scoreValue getter
/**
   \return the scoreValue of the FallingImage
 */
int FallingImage::getScoreValue()
{
   return scoreValue;
}

///scoreValue setter
/**
   \param _score the new score_value as an int
 */
void FallingImage::setScoreValue(const int _score)
{
   scoreValue = _score;
}
