#include "GoodFallingImage.h"

/// the speed value of the PowerUpFallingImage initialized with a default Vector(0,0)
Vector GoodFallingImage::speed = Vector(0,0);



///Constructor for the GoodFallingImage
/**
   \param _origin is a Point representing the origin for the GoodFallingImage
   \param picture is an ALLEGRO_BITMAP* which represents the image that will be drawn for this GoodFallingImage
   \param scoreValue is an int representing the value that this GoodFallingImage will increase the score by if caught
 */
GoodFallingImage::GoodFallingImage(const Point& _origin, ALLEGRO_BITMAP* picture, int scoreValue) : FallingImage(_origin, picture, scoreValue)
{}


///draws the GoodFallingImage
/**
   draws the image for the Good Image using its origin and its image.
*/
void GoodFallingImage::draw()
{
   al_draw_bitmap(image, origin.getX(), origin.getY(), 0);
}


///Checks to see if this current GoodFallingImage has been caught
/**
   \param dt is a double which represents the change in time
   \return a bool representing whether the GoodFallingImage should still be displayed
*/
bool GoodFallingImage::isAlive(double dt)
{

   //Check to see if the update will push the image across the line
   translate(speed * dt);
   if (origin.getY() >= 450)
      return false;
   
   return true;

}
///Setter for GoodFallingImage speed
/**
   \param _speed is a const Vector representing the new speed for the GoodFallingImage
 */
void GoodFallingImage::setSpeed(const Vector& _speed)
{
   speed = _speed;
}
