#include "GameBoard.h"
#include <fstream>
#include <iostream>
#include <cmath>
#include "ReverseMovementState.h"
#include "DoubleDudeState.h"
#include "DoubleMultiplierState.h"

///GameBoard constructor
/**
   \param playerImageName is a string that represents the file name for the player character image
   \param backgroundName is a string that represents the file name for the Background image
   \param songName is a string that represents the file name for the song
   \param _fallingImageName is a string that represents the file name for the fallingimage 
   \param speed is a double that represents the speed at which images fall

   This constructor loads all the required bitmaps and checks to make sure that the proper files actually exist. It also sets up the music and states.  
 */
GameBoard::GameBoard(std::string playerImageName, std::string backgroundName, std::string songName, std::string _fallingImageName,  double speed): playerImage(NULL), playerPosition(300), score(0), imageSpeed(speed), multiplier(1), caughtCount(0), fallingImageName(_fallingImageName)
{
   GoodFallingImage::setSpeed(Vector(0,imageSpeed));
 
   PowerUpFallingImage::speed = Vector(0, imageSpeed);
 
   isPlaying = false;
   
   //************************ IMAGES SETUP *********************************//
   setPowerUpImageName("./image/powerUp.png"); 

   if (!(playerImage = al_load_bitmap(playerImageName.c_str())))
      throw (std::runtime_error("Bad Filename: " + playerImageName + " Does Not Exist"));

   
   if(!(background = al_load_bitmap(backgroundName.c_str())))
      throw (std::runtime_error("Bad Filename: " + backgroundName + " Does Not Exist"));

   ALLEGRO_BITMAP* newImage;
   if(!( newImage = al_load_bitmap(fallingImageName.c_str())))
      throw (std::runtime_error("Bad Filename: " + fallingImageName + " Does Not Exist"));

   al_destroy_bitmap(newImage);

   for (int i = 0; i < 5; i++)
      multi[i] = NULL;
   
   string multiplierImages [5];
   
   multiplierImages[0] = "./image/Mult1.png";
   multiplierImages[1] = "./image/Mult2.png";
   multiplierImages[2] = "./image/Mult4.png";
   multiplierImages[3] = "./image/Mult8.png";
   multiplierImages[4] = "./image/Mult16.png";

   setMultiplierImageNames(multiplierImages);
   
   //************************ FONT SETUP *********************************//
   scoreFont = NULL;
   setFontName("./fonts/BRADHITC.TTF");

   //************************ MUSIC SETUP *********************************//
   al_reserve_samples(1);
   
   if(!(song = al_load_sample(songName.c_str())))
      throw (std::runtime_error("Bad Filename: " + songName + " Does Not Exist"));
   
   songInstance = al_create_sample_instance(song);
   ourMixer = al_get_default_mixer();
   al_attach_sample_instance_to_mixer(songInstance, ourMixer);
   al_set_sample_instance_playmode(songInstance, ALLEGRO_PLAYMODE_ONCE);

 
   //************************ STATE SETUP *********************************//
   noPowerUp = new StateInterface();
   reverseMovement = new ReverseMovementState();
   doubleDude = new DoubleDudeState();
   doubleMultiplier = new DoubleMultiplierState();
   
   currentState = noPowerUp;

}

///The destructor handles the destuction of all images, states and songs
GameBoard::~GameBoard()
{
   if(playerImage)
   {
      al_destroy_bitmap(playerImage);
      playerImage = NULL;
   }
   
   if(background)
   {
      al_destroy_bitmap(background);
      background = NULL;
   }

   if(song)
   {
      al_destroy_sample(song);
      song = NULL;
   }

   if(songInstance)
   {
      al_destroy_sample_instance(songInstance);
      songInstance = NULL;
   }
   
   if(!fallingImages.empty())
      for(auto iter : fallingImages)
	 delete iter;

   if(scoreFont)
   {
      al_destroy_font(scoreFont);
      scoreFont = NULL;
   }

   for (int i = 0; i < 4; i++)
      al_destroy_bitmap(multi[i]);

   delete noPowerUp;
   delete reverseMovement;
   delete doubleDude;
   delete doubleMultiplier;
 
}

///Draws all the items for the game
/**
   calls the draw function in the currentState
 */
void GameBoard::draw()
{
   currentState->draw(*this);
}

///Getter for the current score font
/**
   \return an ALLEGRO_FONT* that is the current score Font
 */
ALLEGRO_FONT* GameBoard::getScoreFont() const
{
   return scoreFont;
}

///Getter for the current BackgroundImage
/**
   \return an ALLEGRO_BITMAP* that represents the current background Image
 */
ALLEGRO_BITMAP * GameBoard::getBackgroundImage() const
{
   return background;
}

///Getter for playerImage
/**
   \return an ALLEGRO_BITMAP* that represents the playerImage
 */
ALLEGRO_BITMAP* GameBoard::getPlayerImage() const
{
   return playerImage;
}

///Setter for the current PowerUpState
/**
   \param newState is a int that represents what the new state will be

   0 is noPowerUp
   -1 is reverseMovement
   -2 is doubleDude
   -3 is doubleMultiplier
 */
void GameBoard::setPowerUpState(int newState)
{
   switch(newState)
   {
      case 0:
	 currentState = noPowerUp;
	 break;
      case -1:
	 currentState = reverseMovement;
	 powerUpCountdown = 12;
	 break;
      case -2:
	 currentState = doubleDude;
	 powerUpCountdown = 12;
	 break;
      case -3:
	 currentState = doubleMultiplier;
	 powerUpCountdown = 12;
	 break;
   }

   
}

///Getter for the list of Falling Images
/**
   \return a list of FallingImage* 
 */
list<FallingImage*>& GameBoard::getFallingImages()
{
   return fallingImages;
}

///Getter for playerPosition
/**
   \return an int that represents the playerPosition on the X-Axis
 */
int GameBoard::getPlayerPosition() const
{
   return playerPosition;
}

///Updates the playerImage for drawing
/**
   \param movement is an integer representing the amount the player should be moved

   The players image is moved based on modulus so that it will wrap around
 */
void GameBoard::updatePlayerPosition(int movement)
{
   movement = currentState->updatePlayerPosition(movement);

   if(playerPosition + movement < 0)
      playerPosition = 600 ;
   else
       playerPosition = (playerPosition +  movement) % 750;  
}

///updates the score
/**
   \param valueOfShape is an int that represents the value to be added to the score

   the valueOfShape is multiplied by the multiplier before adding it to the score
 */
void GameBoard::updateScore(int valueOfShape)
{
   score += (valueOfShape * multiplier);
   if (score < 0)
      score = 0; 
}

///Getter for score
/**
   \return an int representing the score 
 */
int GameBoard::getScore() const
{
   return score;
}

///Setter for score
/**
   \param newScore an int representing the newScore that will replace the current score
 */
void GameBoard::setScore(int newScore)
{
   score = newScore;
}

///Setter for PlayerHeight
/**
   \return a double that represents the height of the playerImage
 */
double GameBoard::getPlayerHeight()
{
   return al_get_bitmap_height(playerImage);
}

///Setter for PlayerWidth
/**
   \return a double that represents the width of the playerImage
 */
double GameBoard::getPlayerWidth()
{
   return al_get_bitmap_width(playerImage);
}

///Updates all the FallingImages in the fallingImages list
/**
   calls the updateFallingImage function for the currentState
 */
void GameBoard::updateFallingImages(double dt)
{
   currentState->updateFallingImages(dt, *this);
}

///Adds a GoodFallingImage to the game
/**
   Adds a new GoodFallingImage into the fallingImages list. It's X-Axis position is randomly generated. 
 */
void GameBoard::addFallingImage()
{
   ALLEGRO_BITMAP* newImage = al_load_bitmap(fallingImageName.c_str());
   fallingImages.push_back(new GoodFallingImage(Point((0+(150*(rand() % 4))), -150), newImage, 5));
}

///Adds a PowerUpFallingImage to the game
/**
   Adds a new PowerUpFallingImage into the fallingImages list. It's X-Axis position is randomly generated. 
 */
void GameBoard::addPowerUpFallingImage()
{
   ALLEGRO_BITMAP* newImage = al_load_bitmap(powerUpImageName.c_str());
   fallingImages.push_back(new PowerUpFallingImage(Point((0+(150*(rand() % 4))), -150), newImage));				     
}

///Removes a fallingImage from the list
/**
   \param iter is a iterator for a FallingImage* list.
   \return a FallingImage* list iterator that is the iter position after the iterator that was deleted
   
   Removes the falling image located at the given iterator
 */
list<FallingImage*>::iterator GameBoard::removeFallingImage(list<FallingImage*>::iterator iter)
{
   if (!fallingImages.empty())
   {
      FallingImage* temp = (*iter);
      list<FallingImage*>::iterator tempIter = fallingImages.erase(iter);
      delete temp;
      return tempIter;
   }
}

///Getter for the Length of the Music
/**
   \return a double representing the length of the games song.
 */
double GameBoard::getMusicLength() const
{
   return al_get_sample_instance_time(songInstance);
}

///Getter for the multiplier
/**
   \return a int representing the current multiplier 
 */
int GameBoard::getMultiplier() const
{
   return multiplier;
}

///Setter for the multiplier
/**
   \param newMultiplier is an int representing the new multiplier that will represent the new multiplier 
 */
void GameBoard::setMultiplier(int newMultiplier)
{
   multiplier = newMultiplier;
}

///Getter for the caughtCount
/**
   \return a int that represents the current caughtCount of the game
 */
int GameBoard::getCaughtCount()
{
   return caughtCount;
}

///Setter for the caughtCount
/**
   \param newCaughtCount is the new caught count for the current game
 */
void GameBoard::setCaughtCount(int newCaughtCount)
{
   caughtCount = newCaughtCount;   
}

///Getter for the current PowerUp Countdown
/**
   \return a double that represents the current powerUpCountdown
 */
double GameBoard::getPowerUpCountDown() const
{
   return powerUpCountdown;
}

///Getter for the fallingImage at the back of the list 
/**
   \return a FallingImage* that represents the FallingImage located at the back of the fallingImages list
 */
FallingImage* GameBoard::getFallingImage()
{
   return fallingImages.back();
}

///Getter for the list of Falling Images
/**
   \return a list of FallingImage* 
 */
const list<FallingImage*>& GameBoard::getFallingImages() const
{
   return fallingImages;
}

///Getter for a Multiplier Image
/**
   \param multiplier is an int that represents the multiplier for which the image for it will be returned
   \return a ALLEGRO_BITMAP* that represents the multiplier image for the multiplier that was passed in
 */
ALLEGRO_BITMAP * GameBoard::getMultiplierImage(int multiplier) const
{
   switch(multiplier)
   {
      case 1:
	 return multi[0];
	 break;
      case 2:
	 return multi[1];
	 break;
      case 4:
	 return multi[2];
	 break;
      case 8:
	 return multi[3];
	 break;
      case 16:
	 return multi[4];
	 break;
   }
}

///Setter for the file names for the mulitplier images
/*
  \param multiImgNames is a string of arrays, each element at index i will represent the multiplier image used for 1, 2, 4, 8, 16 score multiplier images

  file names are checked within and exceptions are thrown if those files don't exist or loading them fails
 */
void GameBoard::setMultiplierImageNames(string multiImgNames[])
{
   if(multi[0])
      al_destroy_bitmap(multi[0]);

   if(!(multi[0] = al_load_bitmap(multiImgNames[0].c_str())))
      throw (std::runtime_error("Bad Filename: " + multiImgNames[0] + " Does Not Exist"));

   if(multi[1])
      al_destroy_bitmap(multi[1]);

   if(!(multi[1] = al_load_bitmap(multiImgNames[1].c_str())))
      throw (std::runtime_error("Bad Filename: " + multiImgNames[1] + " Does Not Exist"));
   
   if(multi[2])
      al_destroy_bitmap(multi[2]);

   if(!(multi[2] = al_load_bitmap(multiImgNames[2].c_str())))
      throw (std::runtime_error("Bad Filename: " + multiImgNames[2] + " Does Not Exist"));
   
   if(multi[3])
      al_destroy_bitmap(multi[3]);

   if(!(multi[3] = al_load_bitmap(multiImgNames[3].c_str())))
      throw (std::runtime_error("Bad Filename: " + multiImgNames[3] + " Does Not Exist"));

   if(multi[4])
      al_destroy_bitmap(multi[4]);

   if(!(multi[4] = al_load_bitmap(multiImgNames[4].c_str())))
      throw (std::runtime_error("Bad Filename: " + multiImgNames[4] + " Does Not Exist"));
}

///Setter for the scoreFont
/**
   \param fontName is a string that represents the file name for the font to be used for displaying the score
 */
void GameBoard::setFontName(string fontName)
{
   if(scoreFont)
      al_destroy_font(scoreFont);

   al_set_new_bitmap_flags(ALLEGRO_MEMORY_BITMAP);

   if(!( scoreFont = al_load_font(fontName.c_str(), 64, 0)))
      throw (std::runtime_error("Bad Filename: " + fontName + " Does Not Exist"));

   al_set_new_bitmap_flags(ALLEGRO_VIDEO_BITMAP);
}

///Setter for the powerUp Image Name
/**
   \param powImgName is a string representing the new file name for the powerUpImage

   file name is checked within and exceptions are thrown if the file doesn't exist or loading it fails
 */
void GameBoard::setPowerUpImageName(string powImgName)
{
   powerUpImageName = powImgName;
   ALLEGRO_BITMAP* tempImg;
   if(!(tempImg = al_load_bitmap(powImgName.c_str())))
      throw (std::runtime_error("Bad Filename: " + powImgName + " Does Not Exist"));

   al_destroy_bitmap(tempImg);
   
}

///Mutes or unmutes the music
/**
   \param mute is a bool that represents whether the music should be muted or unmuted
 */
void GameBoard::muteMusic(bool mute)
{
   if(mute)
      al_set_sample_instance_gain(songInstance,0);
   else
      al_set_sample_instance_gain(songInstance,1);
}

///Starts the music for the game
void GameBoard::startMusic()
{
   al_play_sample_instance(songInstance);
   isPlaying = true; 
}

///Function to pause and restart the music
void GameBoard::pauseAndRestart()
{
   isPlaying = !isPlaying;
   al_set_mixer_playing(ourMixer, isPlaying);
}

///Check to see if a new FallingImage should be added
/**
   \return a bool representing whether a new FallingImage should be added or not
 */
bool GameBoard::readyForImage()
{
   
   if (fallingImages.empty())
      return true;
   
   double imageYOrigin = fallingImages.back() -> getOrigin().getY();
   
   if (((int) imageYOrigin  >  -2) && ((int) imageYOrigin  <  13))
      return true;

   else
      return false;
}


///Check to see if there is a PowerUp active
/**
   \return a bool that represents whether or not there is a PowerUp active
 */
bool GameBoard::isPowerUpState()
{
   return !(currentState == noPowerUp);
}

///Decrements the current PowerUp Countdown
/**
   \param dt is a double for which the countdown of the PowerUpCountDown will be decremented by
 */
void GameBoard::decrementCountdown(double dt)
{
   if(powerUpCountdown > 0)
      powerUpCountdown -= dt;
}

