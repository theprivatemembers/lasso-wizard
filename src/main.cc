/** \mainpage
    The Private Members\n
    Jon Burns, Darren Foley, Patrick Lenaour, Jacob Watson\n
    Spring 2014\n
    CPSC 2720 - Practical Software Development\n
    Professor: Robert Benkoczi\n
    Term Project
*/

///\file main.cc

#include <iostream>
#include <allegro5/allegro.h>
#include <allegro5/allegro_primitives.h>
#include <allegro5/allegro_image.h>
#include <allegro5/allegro_audio.h>
#include <allegro5/allegro_acodec.h>
#include "GameEngine.h"
#include "HighScore.h"

/// Driver for the game
int main()
{
   GameEngine game;
   game.gameSession();

   return 0;
}
