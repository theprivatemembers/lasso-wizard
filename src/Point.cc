#include <iostream>
#include "Point.h"
#include "Vector.h"

using namespace std ;

///Initializes a Point.
/**
   If no values are given, the Point object will be initialized with value (0,0)
   \param _x The x coordinate
   \param _y The y coordinate
*/
Point::Point (double _x, double _y) : x(_x), y(_y) {}

///Allows addition of a Point to a Point in the form Point + Point
/**
   Adds rhs to *this (*this + rhs)
   \return The Point sum of *this + rhs
   \param rhs The right-hand side operand of the addition (*this + rhs)
*/
Point Point::operator+ (const Point& rhs) const
{
   return Point(x + rhs.x, y + rhs.y) ;
}

///Allows addition of a Vector to a Point in the form Point + Vector
/**
   Adds rhs to *this (*this + rhs)
   \return The Point with the relative coordinates of the vector applied to it
   \param rhs The right-hand side operand of the addition (*this + rhs)
*/
Point Point::operator+ (const Vector& rhs) const
{
   return Point(x + rhs.getX(), y + rhs.getY()) ;
}

///x coordinate getter
/**
   \return The x coordinate as an double
*/
double Point::getX () const
{
   return x ;
}

///y coordinate getter
/**
   \return The y coordinate as an double
*/
double Point::getY () const
{
   return y ;
}


///Output operator overload, used in the form ostream << Point
/**
   Overloads the output operator(<<) for easy printing
   \return The ostream which was passed into the function as a parameter; allows cascading
   \param out An ostream which is the lefthand operand for the << operator; usually cout
   \param rhs A Point which is the righthand operand for the << operator
*/
ostream& operator<< (ostream& out, const Point& rhs)
{
   out << "(" << rhs.x << "," << rhs.y << ")" ;

   return out ; //for cascading
}
