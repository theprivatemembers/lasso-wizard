#include "StaticFallingImage.h"

///StaticFallingImage constructor
/**
   \param _origin is a Point representing the origin for the StaticFallingImage
   \param _image is an ALLEGRO_BITMAP* which represents the image that will be drawn for this StaticFallingImage
   \param life is a double representing how long this image will be alive
*/
StaticFallingImage::StaticFallingImage(const Point& _origin, ALLEGRO_BITMAP* _image, double life) : FallingImage(_origin, _image), lifetime(life)
{
}

///decrements the lifetime of the StaticFallingImage and checks to see if it is still alive
/**
   \param dt is a double which is a time variable to be subtracted from lifetime
   \return a bool representing whether the lifetime of the StaticFallingImage has expired
*/
bool StaticFallingImage::isAlive(double dt)
{
   decrementLife(dt);
   if (lifetime <= 0)
      return false;

   return true;
}

///decrements the lifetime of the StaticFallingImage
/**
   \param dt is a double which is a time variable to be subtracted from lifetime
*/
void StaticFallingImage::decrementLife(double dt)
{
   lifetime -= dt;
}

///draws the StaticFallingImage
/**
   draws the image for the Static Image using its origin and its image.
*/
void StaticFallingImage::draw()
{
   al_draw_bitmap(image, origin.getX(), origin.getY(), 0);
}
