#include "DoubleDudeState.h"

DoubleDudeState::DoubleDudeState()
{
   if(!(activePowerUpImage = al_load_bitmap("./image/doubleDudePowerUpActive.png")))
      throw (std::runtime_error("Bad Filename: ./image/doubleDudePowerUpActive.png Does Not Exist"));
}

DoubleDudeState::~DoubleDudeState()
{
   al_destroy_bitmap(activePowerUpImage);
}

void DoubleDudeState::updateFallingImages(double dt, GameBoard& board) const
{

   int newOtherX = (board.getPlayerPosition() + 150) % 750;

   if(newOtherX < 0)
      newOtherX = 600;
   
   bool caught = false;
   Point temp;

   board.decrementCountdown(dt);
   if (board.getPowerUpCountDown() <= 0)
      board.setPowerUpState(0);
   
   auto iter = board.getFallingImages().begin();
   while(iter != board.getFallingImages().end())
   {
      if(!((*iter) -> isAlive(dt))) //Image has hit the Y-Axis 
      {
	 
	 if ((((*iter) -> getOrigin().getX() == board.getPlayerPosition()) or newOtherX == (*iter) -> getOrigin().getX()) and ((*iter) -> getScoreValue() > 0)) //if image was caught
	 {
	    board.setCaughtCount(board.getCaughtCount()+1);
      	    if ((board.getCaughtCount() % 10 == 0) and (board.getMultiplier() != 8) and (board.getCaughtCount() != 0))
      	       board.setMultiplier(board.getMultiplier() * 2);
      	    board.muteMusic(false); //turn audio back on in case it was muted
      	    board.updateScore((*iter)->getScoreValue());
	    temp = (*iter) -> getOrigin();
	    caught = true;
	 }
	
	 else if ((*iter) -> getScoreValue() > 0) //if image wasn't caught
      	 {
      	    board.setCaughtCount(0);
      	    board.setMultiplier(1);
      	    board.muteMusic(true); //mute audio
      	 }

	 else if ((((*iter) -> getOrigin().getX()) == board.getPlayerPosition()) and ((*iter) -> getScoreValue() < 0))
	 {
	    //POWERUP!!!!!
	    board.setPowerUpState((*iter) -> getScoreValue());
	 }

	 iter = board.removeFallingImage(iter);
      	 continue;


      }
      iter++;   
   }

   if(caught)
   {
      ALLEGRO_BITMAP* newImage;
      switch(board.getMultiplier())
      {
	 case 1:
	    if (!(newImage = al_load_bitmap("./image/scorePopUp5.png")))
	       throw (std::runtime_error("Bad Filename: ./image/scorePopUp5.png Does Not Exist"));
	    
	    board.getFallingImages().push_front(new StaticFallingImage(temp, newImage, 0.5));
	    break;
	 case 2:
	    if (!(newImage = al_load_bitmap("./image/scorePopUp10.png")))
	       throw (std::runtime_error("Bad Filename: ./image/scorePopUp10.png Does Not Exist"));
	    
	    board.getFallingImages().push_front(new StaticFallingImage(temp, newImage, 0.5));
	    break;
	 case 4:
	    if (!(newImage = al_load_bitmap("./image/scorePopUp20.png")))
	       throw (std::runtime_error("Bad Filename: ./image/scorePopUp20.png Does Not Exist"));
	    
	    board.getFallingImages().push_front(new StaticFallingImage(temp, newImage, 0.5));
	    break;
	 case 8:
	    if (!(newImage = al_load_bitmap("./image/scorePopUp40.png")))
	       throw (std::runtime_error("Bad Filename: ./image/scorePopUp40.png Does Not Exist"));;
	    board.getFallingImages().push_front(new StaticFallingImage(temp, newImage, 0.5));
	    break;
      }
   } 
}

void DoubleDudeState::draw(const GameBoard& board) const
{
   int newOtherX = (board.getPlayerPosition() + 150) % 750;

   if(newOtherX < 0)
      newOtherX = 600;
   
   al_draw_bitmap(board.getBackgroundImage(), 0, 0, 0);
   al_draw_bitmap(board.getPlayerImage(), board.getPlayerPosition(), 600, 0);
   al_draw_tinted_bitmap(board.getPlayerImage(), al_map_rgba_f(.65, .65, .65, 1), newOtherX, 600, 0);

   al_draw_bitmap(board.getMultiplierImage(board.getMultiplier()), 760, 470, 0);
   const list<FallingImage*>& fallingImages = board.getFallingImages();
   for(auto iter : fallingImages)
      iter -> draw();
   
   Text((to_string(board.getScore())), board.getScoreFont(),0, 139, 69,Point(800,400)).print();

   double powerUpCountdownRemainingOverMax = board.getPowerUpCountDown()/12;
   al_draw_tinted_bitmap(activePowerUpImage,al_map_rgba_f(1,1,1,powerUpCountdownRemainingOverMax), 900,650,0);
   
   al_flip_display();

}
