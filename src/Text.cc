#include"Text.h"

/// Text constructor
/**
   \param t is a string that contains the text to be drawn
   \param _font is an ALLEGRO_FONT* which is the font that will be used to draw the text
   \param r is and int representing the red value of the colour of the font 
   \param g is and int representing the green value of the colour of the font 
   \param b is and int representing the blue value of the colour of the font 
   \param o is a Point representing where the Text should be drawn

*/
Text::Text(string t, ALLEGRO_FONT* _font, int r, int g, int b, const Point& o) : font(_font), origin(o), text(t)
{
  color = new ALLEGRO_COLOR(al_map_rgb(r, g, b));
}

/// Text destructor
/**
   destroys the color object
 */
Text::~Text()
{
   if(color)
   {
      delete color;
      color = NULL;
   }
}

///A Getter for the origin of the Text
/**
   \return a Point representing the origin of the Text
*/
Point Text::getOrigin()
{
   return origin;
}

///Setter for the origin of the Text
/**
   \param newOrigin is a const Point& which represents the new origin of the Text
*/
void Text::setOrigin(const Point& newOrigin)
{
   origin = newOrigin;
}

///draws the text to the screen
/**
   uses the color object and the origin.
*/
void Text::print()
{
   al_draw_text(font, *color, origin.getX(), origin.getY(), 0, text.c_str());
}

///Setter for the text of the Text
/**
   \param t is a string representing the new text for Text
*/
void Text::setText(string t)
{
   text = t;
}
