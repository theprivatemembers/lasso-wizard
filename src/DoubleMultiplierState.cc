#include "DoubleMultiplierState.h"

DoubleMultiplierState::DoubleMultiplierState()
{
   if (!(activePowerUpImage = al_load_bitmap("./image/doubleMultiplierPowerUpActive.png")))
      throw (std::runtime_error("Bad Filename: ./image/doubleMultiplierPowerUpActive.png Does Not Exist"));
   
}

DoubleMultiplierState::~DoubleMultiplierState()
{
   al_destroy_bitmap(activePowerUpImage);
}

void DoubleMultiplierState::updateFallingImages(double dt, GameBoard& board) const
{
   bool caught = false;
   Point temp;

   board.decrementCountdown(dt);
   if (board.getPowerUpCountDown() <= 0)
      board.setPowerUpState(0);
   
   auto iter = board.getFallingImages().begin();
   while(iter != board.getFallingImages().end())
   {
      if(!((*iter) -> isAlive(dt))) //Image has hit the Y-Axis 
      {
	 
	 if (((*iter) -> getOrigin().getX() == board.getPlayerPosition()) and ((*iter) -> getScoreValue() > 0)) //if image was caught
	 {
	    board.setCaughtCount(board.getCaughtCount()+1);
      	    if ((board.getCaughtCount() % 10 == 0) and (board.getMultiplier() != 8) and (board.getCaughtCount() != 0))
      	       board.setMultiplier(board.getMultiplier() * 2);
      	    board.muteMusic(false); //turn audio back on in case it was muted
      	    board.updateScore((*iter)->getScoreValue()*2);
	    temp = (*iter) -> getOrigin();
	    caught = true;
	 }
	
	 else if ((*iter) -> getScoreValue() > 0) //if image wasn't caught
      	 {
      	    board.setCaughtCount(0);
      	    board.setMultiplier(1);
      	    board.muteMusic(true); //mute audio
      	 }

	 else if ((((*iter) -> getOrigin().getX()) == board.getPlayerPosition()) and ((*iter) -> getScoreValue() < 0))
	 {
	    //POWERUP!!!!!
	    board.setPowerUpState((*iter) -> getScoreValue());
	 }

	 iter = board.removeFallingImage(iter);
      	 continue;
      }
      iter++;   
   }

   if(caught)
   {
      ALLEGRO_BITMAP* newImage;

      switch(board.getMultiplier() *2)
      {
	 case 2:
	    if (!(newImage = al_load_bitmap("./image/scorePopUp10.png")))
	       throw (std::runtime_error("Bad Filename: ./image/scorePopUp10.png Does Not Exist"));
	    
	    board.getFallingImages().push_front(new StaticFallingImage(temp, newImage, 0.5));
	    break;
	 case 4:
	    if (!(newImage = al_load_bitmap("./image/scorePopUp20.png")))
	       throw (std::runtime_error("Bad Filename: ./image/scorePopUp20.png Does Not Exist"));
	    
	    board.getFallingImages().push_front(new StaticFallingImage(temp, newImage, 0.5));
	    break;
	 case 8:
	    if (!(newImage = al_load_bitmap("./image/scorePopUp40.png")))
	       throw (std::runtime_error("Bad Filename: ./image/scorePopUp40.png Does Not Exist"));;
	    board.getFallingImages().push_front(new StaticFallingImage(temp, newImage, 0.5));
	    break;
	 case 16:
	    if (!(newImage = al_load_bitmap("./image/scorePopUp80.png")))
	       throw (std::runtime_error("Bad Filename: ./image/scorePopUp80.png Does Not Exist"));;
	    board.getFallingImages().push_front(new StaticFallingImage(temp, newImage, 0.5));
	    break;
      }
   }
}

void DoubleMultiplierState::draw(const GameBoard& board) const
{
   al_draw_bitmap(board.getBackgroundImage(), 0, 0, 0);
   al_draw_bitmap(board.getPlayerImage(), board.getPlayerPosition(), 600, 0);

   al_draw_bitmap(board.getMultiplierImage(board.getMultiplier()*2), 760, 470, 0);
   const list<FallingImage*>& fallingImages = board.getFallingImages();
   for(auto iter : fallingImages)
      iter -> draw();
   
   Text((to_string(board.getScore())), board.getScoreFont(),0, 139, 69,Point(800,400)).print();

   double powerUpCountdownRemainingOverMax = board.getPowerUpCountDown()/12;
   al_draw_tinted_bitmap(activePowerUpImage,al_map_rgba_f(1,1,1,powerUpCountdownRemainingOverMax), 900,650,0);
   
   al_flip_display();
   
}
