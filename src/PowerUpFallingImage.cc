#include "PowerUpFallingImage.h"

/// the speed value of the PowerUpFallingImage initialized with a default Vector(0,0)
Vector PowerUpFallingImage::speed = Vector(0,0);

///Constructor for the PowerUpFallingImage
/**
   Construct also sets the score value to a random number between -1 to -3 each representing a State that this PowerUp will place the GameBoard in
   
   \param _origin is a Point representing the origin for the PowerUpFallingImage
   \param _image is an ALLEGRO_BITMAP* which represents the image that will be drawn for this PowerUpFallingImage
 */
PowerUpFallingImage::PowerUpFallingImage(const Point& _origin, ALLEGRO_BITMAP* _image) : FallingImage(_origin, _image)
{
   setScoreValue(-(rand() % 3 + 1));
}

///draws the PowerUpFallingImage
/**
   draws the image for the PowerUp using its origin and its image.
*/
void PowerUpFallingImage::draw()
{
   al_draw_bitmap(image, origin.getX(), origin.getY(), 0);
}

///Checks to see if this current PowerUpFallingImage has been caught
/**
   \param dt is a double which represents the change in time
   \return a bool representing whether the PowerUp Image should still be displayed
*/
bool PowerUpFallingImage::isAlive(double dt)
{
   //Check to see if the update will push the image across the line
   translate(speed * dt);
   if (origin.getY() >= 450)
      return false;
   
   return true;

}
