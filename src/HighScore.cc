#include "HighScore.h"
#include <stdlib.h>
#include <fstream>
#include <iostream>

///HighScores constructor
/**
   Initializes all of the high scores from the HighScore.txt file
*/
HighScores::HighScores()
{
   for(int i = 0; i < 4; i++)
      scores.push_back(vector<score>());
   initializeScores();
}

///HighScores destructor
/**
   Writes all of the current high scores to the HighScore.txt file
*/
HighScores::~HighScores()
{
   ofstream file;

   
   file.open("HighScore.txt", ios::trunc);

   for(int i = 0; i < scores.size(); i++)
   {
      for (int j = 0; j < scores[i].size(); j++)
      {
   	 file << to_string((scores[i])[j].level) + "\n";
	 
   	 file << (scores[i])[j].name + "\n";

   	 file << to_string((scores[i])[j].score) + "\n";
      }
   }

   file.close();
}

///The highscores are loaded from a HighScore.txt file into a vector
void HighScores::initializeScores()
{
   ifstream myFile;

   myFile.open("HighScore.txt");

   string input;

   while (getline(myFile, input))
   {
      score tempScore;
	 
      if (myFile.eof())
	 break;

      tempScore.level = atoi(input.c_str());

      getline(myFile, input);
      tempScore.name = input;

      getline(myFile, input);
      tempScore.score = atoi(input.c_str());
      scores[tempScore.level].push_back(tempScore);
   }

   myFile.close();
}

///Function for getting the HighScores for a given level
/**
   \param level The level for which HighScores should be returned
   \return a score vector corresponding to the passed level value
*/
const vector<score>& HighScores::getScore(int level)
{
   return scores[level];
}

///function which sorts the HighScores for a given level
/**
   \param level - the level to be sorted

   The vector for the given level is sorted in reverse order so that the
   values can be stored
*/
void HighScores::sort(int level)
{
   vector<score> &currentLevelScores = scores[level];

   for(size_t i = 1; i < currentLevelScores.size(); i++)
   {
      int j = i;
      while (j > 0 and currentLevelScores[j - 1].score < currentLevelScores[j].score)
      {
	 swap(currentLevelScores[j], currentLevelScores[j - 1]);
	 j--;
      }
   }

   //restrict number of scores saved
   while(currentLevelScores.size() > 7)
      currentLevelScores.pop_back();
}


///swaps one score with another
/**
   \param item1 The first item (a score) to be swapped
   \param item2 The second item (a score) to be swapped

   The passed scores will be swapped with eachother
*/
void HighScores::swap(score& item1, score& item2)
{
   score temp = item1;
   item1 = item2;
   item2 = temp;
}

///Function for adding a new HighScore
/**
   \param newScore The score to add
   \param level The level to which the HighScore belongs

   newScore is added to the appropriate vector based on the level passed
*/
void HighScores::addScore(score newScore, int level)
{
   scores[level].push_back(newScore);
   sort(level);
}
