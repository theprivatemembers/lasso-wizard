#include "ReverseMovementState.h"

///Default Constructor for the ReverseMovementState
ReverseMovementState::ReverseMovementState()
{
   if (!(activePowerUpImage = al_load_bitmap("./image/reverseKeysPowerUpActive.png")))
      throw (std::runtime_error("Bad Filename: ./image/reverseKeysPowerUpActive.png Does Not Exist"));
}

///Destructor for ReverseMovementState, destroys the ALLEGRO_BITMAP* 
ReverseMovementState::~ReverseMovementState()
{
   al_destroy_bitmap(activePowerUpImage);
}

///calculates the amount by which the player position should be updated 
/**
   \param movement is an int representing the distance the player should move
   \return movement reversed which will reverse the controls
*/
int ReverseMovementState::updatePlayerPosition(int movement) const
{
   return -movement;
}

///Draws all of the items in GameBoard. 
/**
   \param board the GameBoard currently in play
   Handles the drawing of the Background, the player Image, the multiplier image, the Score and the falling Images. Also handles the drawing of the active power up image which becomes more transparent over time.
 */
void ReverseMovementState::draw(const GameBoard& board) const
{
   al_draw_bitmap(board.getBackgroundImage(), 0, 0, 0);
   al_draw_bitmap(board.getPlayerImage(), board.getPlayerPosition(), 600, 0);

   al_draw_bitmap(board.getMultiplierImage(board.getMultiplier()), 760, 470, 0);
   const list<FallingImage*>& fallingImages = board.getFallingImages();
   for(auto iter : fallingImages)
      iter -> draw();
   
   Text((to_string(board.getScore())), board.getScoreFont(),0, 139, 69,Point(800,400)).print();

   double powerUpCountdownRemainingOverMax = board.getPowerUpCountDown()/12;
   al_draw_tinted_bitmap(activePowerUpImage,al_map_rgba_f(1,1,1,powerUpCountdownRemainingOverMax), 900,650,0);
   
   al_flip_display();
}
