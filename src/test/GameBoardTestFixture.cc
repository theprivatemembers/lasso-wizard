#include "GameBoardTestFixture.h"
#include <iostream>

void GameBoardTestFixture::setUp()
{ 
   board = new GameBoard("./image/patrick.png", "./image/patrickBackground.jpg", "./songs/Fleshdiet_Snow.wav", "./image/parrot.png", 100);
   //ALLEGRO_BITMAP* newImage = al_load_bitmap("./image/patrick.png");
   board -> addFallingImage();
 
}

void GameBoardTestFixture::tearDown()
{
   delete board;
}

void GameBoardTestFixture::BadImageFileName()
{
   GameBoard temp("I'm a bad fileName", "./image/patrickBackground.jpg", "./songs/Fleshdiet_Snow.wav", "./image/parrot.png", 100);
}


void GameBoardTestFixture::BadBackGroundImageNameExpception()
{
   GameBoard temp("./image/patrick.png", "dummybackground", "./songs/Fleshdiet_Snow.wav", "./image/parrot.png", 100);
}

void GameBoardTestFixture::BadSongNameException()
{
   GameBoard temp("./image/patrick.png", "./image/patrickBackground.jpg", "BadSongName",  "./image/parrot.png", 100);
}

void GameBoardTestFixture::BadFallingImageName()
{
   GameBoard temp("./image/patrick.png", "./image/patrickBackground.jpg", "./songs/Fleshdiet_Snow.wav", "BadImageName", 100);
}

void GameBoardTestFixture::BadMultiplierImageName()
{
   string badNames[5];
   for(int i=0; i<5; ++i)
      badNames[i] = "badName";
      
   board->setMultiplierImageNames(badNames);
}

void GameBoardTestFixture::BadFontName()
{
   board->setFontName("badName");
}

void GameBoardTestFixture::BadPowerUpImageName()
{
   board->setPowerUpImageName("badName");
}

void GameBoardTestFixture::playerImageCreationCheck()
{
   CPPUNIT_ASSERT(board -> getPlayerImage());
}

void GameBoardTestFixture::playerSpawnsInMiddle()
{
   
   CPPUNIT_ASSERT(board -> getPlayerPosition() == 300);
}

void GameBoardTestFixture::updatePlayerPositionMovesPlayer()
{
   
   board -> updatePlayerPosition(100);
   CPPUNIT_ASSERT(board -> getPlayerPosition() == 400);
}

void GameBoardTestFixture::playerDoesNotMovePastBoundaries()
{
   
   board -> updatePlayerPosition(150); //Move once right: 450
   board -> updatePlayerPosition(150); //Move once right: 600
   board -> updatePlayerPosition(150); //Move once right: Should be 0
   CPPUNIT_ASSERT(board -> getPlayerPosition() == 0);
}

void GameBoardTestFixture::updateScoreWithPositive()
{
   
   board -> updateScore(100);
   CPPUNIT_ASSERT(board -> getScore() == 100); 
}

void GameBoardTestFixture::scoreNeverGoesBelowZero()
{
   board -> updateScore(-100);
   CPPUNIT_ASSERT(board -> getScore() >= 0);
}

void GameBoardTestFixture::FallingImageCreationCheck()
{
   CPPUNIT_ASSERT(board -> getFallingImage());
}

void GameBoardTestFixture::FallingImageIsFallingOver1Second()
{
   double tempPos = board -> getFallingImage() -> getOrigin().getY();
   board -> updateFallingImages(1);
   CPPUNIT_ASSERT(board -> getFallingImage() -> getOrigin().getY() > tempPos);
}

void GameBoardTestFixture::FallingImageFallsBy100Over2SecondsWithSpeed50()
{
   delete board;
   board = new GameBoard("./image/patrick.png", "./image/patrickBackground.jpg", "./songs/Fleshdiet_Snow.wav", "./image/parrot.png", 50);
   board -> addFallingImage();
   double tempPos = board -> getFallingImage() -> getOrigin().getY();
   board -> updateFallingImages(2);
   CPPUNIT_ASSERT((board -> getFallingImage() -> getOrigin().getY()) - tempPos == 100);
}

void GameBoardTestFixture::FallingImageIncreasesScoreWhenReachesPlayer()
{
   int tempScore = board -> getScore();
   double tempYPos = 800 - board->getPlayerHeight() - board -> getFallingImage() -> getHeight();
   board -> getFallingImage() -> setOrigin(Point(300,tempYPos));
   board -> updateFallingImages(0) ;
   CPPUNIT_ASSERT(board -> getScore() > tempScore);
}
   
void GameBoardTestFixture::FallingImageFallsBy120Over2SecondsWithSpeed60()
{
   GameBoard tempBoard("./image/patrick.png", "./image/patrickBackground.jpg", "./songs/Fleshdiet_Snow.wav", "./image/parrot.png", 60);
   
   //ALLEGRO_BITMAP* newImage1 = al_load_bitmap("./image/patrick.png");
   tempBoard.addFallingImage();
   
   double tempPos = tempBoard.getFallingImage() -> getOrigin().getY();

   tempBoard.updateFallingImages(2);
   CPPUNIT_ASSERT((tempBoard.getFallingImage() -> getOrigin().getY()) - tempPos == 120);
}

void GameBoardTestFixture::GoodFallingImageWithScoreValue5IncreasesScoreTo5WhenCaught()
{
   int tempScore = board -> getScore();
   double tempYPos = 800 - board->getPlayerHeight()
      - board -> getFallingImage() -> getHeight();
   board -> addFallingImage();
   board -> getFallingImage() -> setOrigin(Point(300,tempYPos));
   board -> updateFallingImages(0) ;

   CPPUNIT_ASSERT((board -> getScore() - tempScore) ==5);
}

void GameBoardTestFixture::TwoGoodFallingImagesWithScoreValues5And5IncreaseScoreTo10WhenBothAreCaught()
{
 
   //ALLEGRO_BITMAP* newImage1 = al_load_bitmap("./image/patrick.png");

   
   board -> addFallingImage();
   int tempScore = board -> getScore();

   double tempYPos = 800 - board->getPlayerHeight()
      - board -> getFallingImage() -> getHeight();

   board -> getFallingImage() -> setOrigin(Point(300,tempYPos));
   board -> updateFallingImages(0) ;
   board -> addFallingImage();
   board -> getFallingImage() -> setOrigin(Point(300,tempYPos));

   board -> updateFallingImages(0) ;
   CPPUNIT_ASSERT((board -> getScore() - tempScore) == 10);
 
}

void GameBoardTestFixture::GoodFallingImagesThatCrossTheXBoundaryAndAreNotCaughtDoNotUpdateScore()
{
   double tempYPos = 800 - board->getPlayerHeight()
      - board -> getFallingImage() -> getHeight();
   
   int tempScore = board -> getScore();
   
   board -> getFallingImage() -> setOrigin(Point(0,tempYPos));
   board -> updateFallingImages(0) ;
   CPPUNIT_ASSERT(tempScore == board -> getScore());
   
}


void GameBoardTestFixture::TwoGoodFallingImagesWithScoreValues5and5StartingAboveBoundaryIncreaseScoreBy10WhenBothAreCaught()
{
   double tempYPos = 800 - board->getPlayerHeight()
      - board -> getFallingImage() -> getHeight();

   int tempScore = board -> getScore();
   
   board -> getFallingImage() -> setOrigin(Point(300, tempYPos - 20));
   board -> updateFallingImages(2);
   
   //ALLEGRO_BITMAP* newImage1 = al_load_bitmap("./image/patrick.png");
   board -> addFallingImage();
   board -> getFallingImage() -> setOrigin(Point(300, tempYPos - 20));
   board -> updateFallingImages(2);
 
   

  

   CPPUNIT_ASSERT((board -> getScore() - tempScore) == 10);


   
}
