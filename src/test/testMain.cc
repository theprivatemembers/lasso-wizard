 #include "GameBoardTestFixture.h"
#include <cppunit/ui/text/TestRunner.h>
#include "GameEngine.h"

int main()
{
   GameEngine();
 
   CppUnit::TextUi::TestRunner runner;

   runner.addTest(GameBoardTestFixture::suite());
 
   runner.run();

   return 0;
}
