CXX = /opt/centos/devtoolset-1.1/root/usr/bin/g++
CFLAGS = -I ./include/ -I /home/lib2720/allegro5/include/ -I ./include/test -Wall -g -std=c++11
LIBDIR = /home/lib2720/allegro5/lib/
LNFLAGS = -lallegro -lallegro_primitives -ldl -lallegro_audio -lallegro_acodec -lallegro_image -lallegro_font -lallegro_ttf -lcppunit 
TESTLNFLAGS= $(LNFLAGS) -lcppunit
OBJECTS = Point.o Vector.o FallingImage.o GameEngine.o GameBoard.o GoodFallingImage.o Text.o HighScore.o StaticFallingImage.o StateInterface.o ReverseMovementState.o DoubleDudeState.o DoubleMultiplierState.o PowerUpFallingImage.o
MAINOBJS = ./obj/main.o $(OBJS)
TESTOBJECTS = GameBoardTestFixture.o testMain.o
SRCDIR = ./src
OBJDIR = ./obj
TESTOBJDIR = ./obj/test
OBJS = $(addprefix $(OBJDIR)/, $(OBJECTS))
TESTOBJS =  $(addprefix $(TESTOBJDIR)/, $(TESTOBJECTS)) $(OBJS)

game: $(MAINOBJS)
	$(CXX) -L $(LIBDIR) -o $@ $^ $(LNFLAGS)

tests: $(TESTOBJS)
	$(CXX) -L $(LIBDIR) -o $@ $^ $(TESTLNFLAGS)

$(OBJDIR)/%.o : $(SRCDIR)/%.cc
	$(CXX) -c -o $@ $< $(CFLAGS)

.PHONY : clean
clean: 
	rm -f $(OBJDIR)/*.o 
	rm -f $(OBJDIR)/test/*.o
	find ./ -name \*\~ -delete -o -name \*\# -delete

.PHONY : clean-all
clean-all: clean
	rm -f game
	rm -f tests

.PHONY : makey
makey: clean-all

.PHONY : meTacos
meTacos: makey
	doxygen